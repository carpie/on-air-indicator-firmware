# pylint: disable=import-error,missing-module-docstring,no-member,broad-except
import sys
import machine

from onair import OnAir

try:
    oa = OnAir(5) # D1
    oa.run()
except KeyboardInterrupt:
    pass
except Exception as e:
    print(e)
    sys.print_exception(e)
    machine.reset()
