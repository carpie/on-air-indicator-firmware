# pylint: disable=import-error,missing-module-docstring
import machine
import utime as time

from umqtt.simple import MQTTClient

import config
from easynet import (
    enable_access_point, connect_to_network, disconnect_from_network, is_network_connected
)

enable_access_point(False)

class OnAir:
    """OnAir controller"""

    def __init__(self, pin):
        self.oa_pin = machine.Pin(pin, machine.Pin.OUT, value=0)
        self.client = None
        disconnect_from_network()

    def on_network_connect(self, err):
        """Callback for network connect"""
        if err:
            print('Error connecting to network', err)
        else:
            self.connect_to_mqtt()

    def on_mqtt_message(self, topic, msg):
        """Callback for mqtt message"""
        decoded_topic = topic.decode()
        if decoded_topic == config.MQTT_SUB_ONAIR_TOPIC:
            if msg == b'on':
                self.oa_pin.value(1)
                self.client.publish(config.MQTT_PUB_LED_STATUS, b'on', retain=True)
            elif msg == b'off':
                self.oa_pin.value(0)
                self.client.publish(config.MQTT_PUB_LED_STATUS, b'off', retain=True)

    def connect_to_mqtt(self):
        """Connect to MQTT server"""
        print(' -- Spinning up MQTT client...')
        self.client = MQTTClient(config.MQTT_CLIENT_ID, config.MQTT_SERVER, keepalive=10)
        self.client.set_callback(self.on_mqtt_message)
        self.client.set_last_will(config.MQTT_PUB_CONNECTED_TOPIC, b'false', retain=True)
        try:
            if not is_network_connected():
                return False
            self.client.connect()
            self.client.publish(config.MQTT_PUB_CONNECTED_TOPIC, b'true', retain=True)
            self.client.subscribe(config.MQTT_SUB_ONAIR_TOPIC)
        except OSError as err:
            print(err)
            return False
        return True


    def run(self):
        """Main loop"""
        ping_timeout = 50
        ping_counter = 0
        while True:
            if not is_network_connected():
                connect_to_network(self.on_network_connect)
                continue
            if self.client:
                self.client.check_msg()
                time.sleep_ms(100)
                # Ping to keep us alive
                ping_counter += 1
                if ping_counter % ping_timeout == 0:
                    self.client.ping()
