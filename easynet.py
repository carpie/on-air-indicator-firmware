# uPython library making connecting to network things easy
import network
import utime as time

import easynet_config as config

wlan = network.WLAN(network.STA_IF)

def enable_access_point(enable):
    ap = network.WLAN(network.AP_IF)
    ap.active(enable)

def connect_to_network(callback, timeout=None):
    print(' -- WLAN status: {}'.format(wlan.status()))
    if wlan.active():
        print(' -- Resetting NIC')
        wlan.active(False)
        time.sleep_ms(250)
    print(' -- Connecting to network...')
    wlan.active(True)
    wlan.connect(config.WIFI_SSID, config.WIFI_PSK)
    wlan.ifconfig((config.NET_IP, config.NET_NETMASK, config.NET_GATEWAY, config.NET_DNS))
    retries = timeout
    while not wlan.isconnected():
        print(' -- WLAN status: {}'.format(wlan.status()))
        if retries is not None:
            retries -= 1
            if retries <= 0:
                return callback('Timed out waiting for network')
        time.sleep(1)
    callback(None)

def disconnect_from_network(): #pragma no cover
    print(' -- Disconnecting from network...');
    wlan.disconnect()
    wlan.active(False)

def is_network_connected():
    return wlan.isconnected();
